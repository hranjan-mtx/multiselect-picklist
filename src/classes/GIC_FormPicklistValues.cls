/**
 * Created by hardikranjan on 29/03/20.
 */

public with sharing class GIC_FormPicklistValues {

    @AuraEnabled
    public static List<Account> getAccounts() {
        return [SELECT Id, Name FROM Account LIMIT 50];
    }

}
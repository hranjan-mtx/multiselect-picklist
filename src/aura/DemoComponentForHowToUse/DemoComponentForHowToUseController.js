/**
 * Created by hardikranjan on 29/02/20.
 */

({

    doInit : function(component, event, helper) {

		let valuesTemp = [];
		valuesTemp.push(
		    { id : '1', name : 'Hello'},
		    { id : '2', name : 'World'},
		    { id : '3', name : 'Loaded'},
		    { id : '4', name : 'Picklist'},
		    { id : '5', name : 'Values'}
		);


		component.set("v.values", valuesTemp);
		component.set('v.pageLoaded', true);

       // Call Apex method to get all picklist values

    },

    onValueSelectFromPicklist : function(component, event, helper) {
		// implement logic after selection if picklist value

		console.log('---values selected--- ' , component.get('v.selectedTopicIds'));

    }

});
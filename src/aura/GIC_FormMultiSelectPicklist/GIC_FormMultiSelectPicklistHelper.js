({
    // Methods to add/remove selections from available and seleted array values
  refeshSelectionData: function(component, event) {

    var _topics = component.get("v.topics");
    var _selectedTopicIds = component.get("v.selectedTopicIds");
    var _selectedTopics = [];
    var _availableTopics = [];

    if (_topics) {
      for (var i = 0; i < _topics.length; i++) {
        if ( _selectedTopicIds.length > 0 && _selectedTopicIds.indexOf(_topics[i].id) > -1) {
             _selectedTopics.push(_topics[i]);
        } else {
            _availableTopics.push(_topics[i]);
        }
      }
    }


    component.set("v.availableTopics", _availableTopics);
    component.set("v.selectedTopics", _selectedTopics);
  },


  // Fire event on value change
  fireEventChange: function(component, event) {
    var compEvent = component.getEvent("picklistChangeEvent");
    compEvent.fire();
  }
});
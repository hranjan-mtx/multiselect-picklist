({

  doInit: function(component, event, helper) {
    helper.refeshSelectionData(component, event);
  },


  doSelection: function(component, event, helper) {
    component.set("v.isOptionsVisible", false);
    let _selectedTopicIds = component.get("v.selectedTopicIds");
    if (!_selectedTopicIds) _selectedTopicIds = [];

    _selectedTopicIds.push(event.currentTarget.id);

    component.set("v.selectedTopicIds", _selectedTopicIds);
    helper.refeshSelectionData(component, event);
    helper.fireEventChange(component, event);
  },


  removeSelection: function(component, event, helper) {
    let _selectedTopicIds = component.get("v.selectedTopicIds");
    _selectedTopicIds.splice(
      _selectedTopicIds.indexOf(event.getSource().get("v.name")),
      1
    );
    component.set("v.selectedTopicIds", _selectedTopicIds);
    helper.refeshSelectionData(component, event);
    helper.fireEventChange(component, event);
  },


  showOptions: function(component, event, helper) {
    component.set("v.isOptionsVisible", true);
  },


  hideOptions: function(component, event, helper) {
    component.set("v.isOptionsVisible", false);
  },

});